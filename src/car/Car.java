/**
 * @author elu - Esko Heino
 * Car.java
 * Date: 9 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package car;

/**
 *
 */
public class Car {
	public Car() {
		Body body = new Body();
		Chassis chassis = new Chassis();
		Engine engine = new Engine();
		Wheel wheel1 = new Wheel();
		Wheel wheel2 = new Wheel();
		Wheel wheel3 = new Wheel();
		Wheel wheel4 = new Wheel();
	}

	public void print() {
		System.out.println("Autoon kuuluu:");
		System.out.println("\tBody");
		System.out.println("\tChassis");
		System.out.println("\tEngine");
		System.out.println("\t4 Wheel");
	}

}

class Body {
	public Body() {
		String className = this.getClass().getSimpleName();
		System.out.println("Valmistetaan: " + className);
	}

}

class Chassis {
	public Chassis() {
		String className = this.getClass().getSimpleName();
		System.out.println("Valmistetaan: " + className);
	}

}

class Engine {
	public Engine() {
		String className = this.getClass().getSimpleName();
		System.out.println("Valmistetaan: " + className);
	}

}

class Wheel {
	public Wheel() {
		String className = this.getClass().getSimpleName();
		System.out.println("Valmistetaan: " + className);
	}

}