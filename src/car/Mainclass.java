/**
 * @author elu - Esko Heino
 * Mainclass.java
 * Date: 9 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package car;

/**
 *
 */
public class Mainclass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Car car1 = new Car();
		car1.print();

	}

}
