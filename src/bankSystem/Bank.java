/**
 * @author elu - Esko Heino
 * Bank.java
 * Date: 23 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package bankSystem;

import java.util.ArrayList;

/**
 *
 */
public class Bank {
		private static ArrayList<Account> accounts = new ArrayList<Account>();

	public Bank(String aN, int a, boolean c, int cA) {
		if (c) {
			accounts.add(new creditAccount(aN, a, cA));
		} else {
			accounts.add(new regularAccount(aN, a));
		}

	}

	public void deposit(String aN, int aTA) {
        for(Account acc : accounts) {
    			if (acc.accountNumber.equals(aN)) {
    				acc.deposit(aTA);
    			}
    }
	}

	public void withdraw(String aN, int aTW) {
        for(Account acc : accounts) {
        		if (acc.accountNumber.equals(aN)) {
        			acc.withdraw(aTW);
        		}
        }
	}

	public void print(String aN) {
        for(Account acc : accounts) {
    			if (acc.accountNumber.equals(aN)) {
    				System.out.println("Tilinumero: " + acc.getAccountNumber() + " Tilillä rahaa: " + acc.getAmount());
    			}
        }
	}

	public void printAll() {
		System.out.println("Kaikki tilit:");
        for(Account acc : accounts) {
        		if (acc.getCreditAmount() == 0) {
        			System.out.println("Tilinumero: " + acc.getAccountNumber() + " Tilillä rahaa: " + acc.getAmount());
        		} else {
        			System.out.println("Tilinumero: " + acc.getAccountNumber() + " Tilillä rahaa: " + acc.getAmount() + " Luottoraja: " + acc.getCreditAmount());
        		}
        }
	}

	public void delete(String aN) {
		int accN = 0;
        for(Account acc : accounts) {
    			if (acc.accountNumber.equals(aN)) {
    				accounts.remove(accN);
    			}
    			accN += 1;
        }
		System.out.println("Tili poistettu.");
	}

}