/**
 * @author elu - Esko Heino
 * Account.java
 * Date: 23 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package bankSystem;

/**
 *
 */
public abstract class Account {
	protected String accountNumber;
	protected int amount;
	protected int creditAmount;

	public Account(String aN, int a, int cA) {
		accountNumber = aN;
		amount = a;
		creditAmount = cA;
		System.out.println("Tili luotu.");
	}

	protected String getAccountNumber() {
		return this.accountNumber;
	}

	protected int getAmount() {
		return this.amount;
	}

	protected int getCreditAmount() {
		return this.creditAmount;
	}

	protected void deposit(int aTA) {
		this.amount += aTA;
	}

	protected void withdraw(int aTW) {
		if (this.amount >= aTW) {
			this.amount -= aTW;
		} else if (this.creditAmount > 0 && this.amount + this.creditAmount >= aTW) {
			this.amount -= aTW;
		} else {
			System.out.println("Tilillä ei ole katetta.");
		}
	}

}

class regularAccount extends Account {
	public regularAccount(String aN, int a) {
		super(aN, a, 0);
	}
}

class creditAccount extends Account {
	public creditAccount(String aN, int a, int cA) {
		super(aN, a, cA);
	}
}