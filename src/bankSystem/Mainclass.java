/**
 * @author elu - Esko Heino
 * Mainclass.java
 * Date: 22 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package bankSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 */
public class Mainclass {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String userInput;
		int selection;
		String accountNumber;
		int amount;
		int loanLimit;
		int withdrawAmount;
		int amountToAdd;

		Bank bank = null;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        loop: while (true) {
	    		System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
	    		System.out.println("1) Lisää tavallinen tili");
	    		System.out.println("2) Lisää luotollinen tili");
	    		System.out.println("3) Tallenna tilille rahaa");
	    		System.out.println("4) Nosta tililtä");
	    		System.out.println("5) Poista tili");
	    		System.out.println("6) Tulosta tili");
	    		System.out.println("7) Tulosta kaikki tilit");
	    		System.out.println("0) Lopeta");
	    		System.out.print("Valintasi: ");
	    		userInput = br.readLine();
	    		selection = Integer.parseInt(userInput);

	    		switch (selection) {
	    			case 0:	break loop;
	    			case 1:
		    	    		System.out.print("Syötä tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		System.out.print("Syötä rahamäärä: ");
		    	    		amount = Integer.parseInt(br.readLine());
		    	    		bank = new Bank(accountNumber, amount, false, 0);
	            		break;
	    			case 2:
		    	    		System.out.print("Syötä tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		System.out.print("Syötä rahamäärä: ");
		    	    		amount = Integer.parseInt(br.readLine());
		    	    		System.out.print("Syötä luottoraja: ");
		    	    		loanLimit = Integer.parseInt(br.readLine());
		    	    		bank = new Bank(accountNumber, amount, true, loanLimit);
	            		break;
	    			case 3:
		    	    		System.out.print("Syötä tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		System.out.print("Syötä rahamäärä: ");
		    	    		amountToAdd = Integer.parseInt(br.readLine());
		    	    		bank.deposit(accountNumber, amountToAdd);
	            		break;
	    			case 4:
		    	    		System.out.print("Syötä tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		System.out.print("Syötä rahamäärä: ");
		    	    		withdrawAmount = Integer.parseInt(br.readLine());
		    	    		bank.withdraw(accountNumber, withdrawAmount);
	            		break;
	    			case 5:
		    	    		System.out.print("Syötä poistettava tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		bank.delete(accountNumber);
	            		break;
	    			case 6:
    	    				System.out.print("Syötä tulostettava tilinumero: ");
		    	    		accountNumber = br.readLine();
		    	    		bank.print(accountNumber);
	            		break;
	    			case 7:
	    	    			bank.printAll();
	            		break;
	    			default:
	            		System.out.println("Valinta ei kelpaa.");;
	            		break;
	        }

        }

	}

}