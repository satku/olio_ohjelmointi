/**
 * @author elu - Esko Heino
 * Mainclass.java
 * Date: 1 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package pulloautomaatti;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 */
public class Mainclass {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String userInput;
		String input;
		int selection;
		int bottleNumber;

		BottleDispenser user1 = new BottleDispenser();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        loop: while (true) {
        		System.out.println("\n*** LIMSA-AUTOMAATTI ***");
        		System.out.println("1) Lisää rahaa koneeseen");
        		System.out.println("2) Osta pullo");
        		System.out.println("3) Ota rahat ulos");
        		System.out.println("4) Listaa koneessa olevat pullot");
        		System.out.println("0) Lopeta");
        		System.out.print("Valintasi: ");
        		userInput = br.readLine();
        		selection = Integer.parseInt(userInput);

        		switch (selection) {
        			case 0:	break loop;
	            case 1:  user1.addMoney();
	            			break;
	            case 2:	user1.returnBottles();
	            			System.out.print("Valintasi: ");
	            			input = br.readLine();
        					bottleNumber = Integer.parseInt(input);
	            			user1.buyBottle(bottleNumber);
	                     break;
	            case 3:  user1.returnMoney();
	                     break;
	            case 4:	user1.returnBottles();
	            			break;
	            default: System.out.println("Väärä valinta!");;
	                     break;
	        }

        }

	}

}