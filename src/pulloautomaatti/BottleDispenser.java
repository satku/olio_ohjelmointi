/**
 * @author elu - Esko Heino
 * BottleDispenser.java
 * Date: 1 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package pulloautomaatti;

import java.util.ArrayList;
import java.util.Locale;

/**
 *
 */
public class BottleDispenser {

	private int bottles;
	private float money;
	private ArrayList<Bottle> bottle_arrayList = new ArrayList<Bottle>();

	public BottleDispenser() {
		bottles = 6;
		money = 0;
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Pepsi Max", 0.5, 1.8));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Pepsi Max", 1.5, 2.2));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
		}
		for (int bottleIndex = 0; bottleIndex < 2; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Fanta Zero", 0.5, 1.95));
		}
	}

	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}

	public void buyBottle(int number) {
		if (bottles == 0 || bottle_arrayList.get(number - 1).getPrice() > money) {
			System.out.println("Syötä rahaa ensin!");
		} else {
			bottles -= 1;
			money -= bottle_arrayList.get(number - 1).getPrice();
			System.out.println("KACHUNK! " + bottle_arrayList.get(number - 1).getName() + " tipahti masiinasta!");
			bottle_arrayList.remove(number - 1);
		}
	}

	public void returnMoney() {
		System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + String.format(Locale.GERMAN, "%.2f", money) + "€");
		money = 0;
	}

	public void returnBottles() {
		for (int bottleIndex = 0; bottle_arrayList.size() > bottleIndex; bottleIndex++) {
			System.out.println(bottleIndex + 1 + ". Nimi: " + bottle_arrayList.get(bottleIndex).getName());
			System.out.println("\tKoko: " + bottle_arrayList.get(bottleIndex).getSize() + "\tHinta: " + bottle_arrayList.get(bottleIndex).getPrice());
		}
	}

}