/**
 * @author elu - Esko Heino
 * Character.java
 * Date: 10 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package fightingSimulator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 */
public abstract class Character {
	String userInput;
	int selection;
	WeaponBehavior wep1 = null;

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

	public Character() throws IOException {
		System.out.println("Valitse aseesi: ");
		System.out.println("1) Veitsi");
		System.out.println("2) Kirves");
		System.out.println("3) Miekka");
		System.out.println("4) Nuija");
		System.out.print("Valintasi: ");
		userInput = br.readLine();
		selection = Integer.parseInt(userInput);

		switch (selection) {
			case 1:
				wep1 = new KnifeBehavior();
				break;
			case 2:
				wep1 = new AxeBehavior();
				break;
			case 3:
				wep1 = new SwordBehavior();
				break;
			case 4:
				wep1 = new ClubBehavior();
				break;
		}
	}

	protected void fight() {
		String fighterName = this.getClass().getSimpleName();
		String strWeapon = wep1.useWeapon();
		String weaponName = strWeapon.replace("Behavior", "");
		System.out.println(fighterName + " tappelee aseella " + weaponName);
	}

}

abstract class WeaponBehavior {
	protected String useWeapon() {
		return this.getClass().getSimpleName();
	}
}

class Knight extends Character {
	public Knight() throws IOException   {
		super();
	}
}

class King extends Character {
	public King() throws IOException   {
		super();
	}
}

class Queen extends Character {
	public Queen() throws IOException   {
		super();
	}
}

class Troll extends Character {
	public Troll() throws IOException   {
		super();
	}
}

class KnifeBehavior extends WeaponBehavior {
	public KnifeBehavior() {
		super();
	}
}

class AxeBehavior extends WeaponBehavior {
	public AxeBehavior() {
		super();
	}
}

class SwordBehavior extends WeaponBehavior {
	public SwordBehavior() {
		super();
	}
}

class ClubBehavior extends WeaponBehavior {
	public ClubBehavior() {
		super();
	}
}