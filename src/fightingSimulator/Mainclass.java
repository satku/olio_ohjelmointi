/**
 * @author elu - Esko Heino
 * Mainclass.java
 * Date: 10 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package fightingSimulator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 */
public class Mainclass {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String userInput;
		int selection;
		int input;
		Character char1 = null;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        loop: while (true) {
	    		System.out.println("*** TAISTELUSIMULAATTORI ***");
	    		System.out.println("1) Luo hahmo");
	    		System.out.println("2) Taistele hahmolla");
	    		System.out.println("0) Lopeta");
	    		System.out.print("Valintasi: ");
	    		userInput = br.readLine();
	    		selection = Integer.parseInt(userInput);

	    		switch (selection) {
	    			case 0:	break loop;
	    			case 1:
	    				System.out.println("Valitse hahmosi: ");
	    				System.out.println("1) Kuningas");
	    				System.out.println("2) Ritari");
	    				System.out.println("3) Kuningatar");
	    				System.out.println("4) Peikko");
	    				System.out.print("Valintasi: ");
	    				userInput = br.readLine();
	    				input = Integer.parseInt(userInput);

	    				switch (input) {
		    				case 1:
		    					char1 = new King();
		    					break;
		    				case 2:
		    					char1 = new Knight();
		    					break;
		    				case 3:
		    					char1 = new Queen();
		    					break;
		    				case 4:
		    					char1 = new Troll();
		    					break;
	    				}
	            		break;
	    			case 2:
	            		char1.fight();
	            		break;
	    			default:
	            		System.out.println("Väärä valinta!");;
	            		break;
	        }
        }
	}
}