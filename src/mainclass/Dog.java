/**
 *
 */
package mainclass;

//import java.util.Scanner;

/**
 * @author elu
 *
 */
public class Dog {
	private String name;
	//private line;

	/*function Dog
	 * makes a new Dog object
	 * input parameters: name for the dog */
	public Dog(String dogName) {
		//line = "Much wow!";
		if ((dogName.trim()).isEmpty()) { //checks if a name was given
			name = "Doge";
		} else {
			name = dogName;
		}
		System.out.println("Hei, nimeni on " + name);
	}

	/*function speak
	 * makes a list of the given sentence
	 * input parameters: a sentence */
	public void speak(String sentence) {

		String []line = sentence.split(" "); //put the given words in a list
		for (String word : line) { //iterate trough the list word by word
			if (word != "") {
				System.out.println(read(word));
			}
		}
		/*while (sc.hasNext()) {
			while (sc.hasNextInt()) {
		        System.out.println("Such integer: " + sc.nextInt());
		    }
		    while (sc.hasNextBoolean()) {
		        System.out.println("Such boolean: " + sc.nextBoolean());
		    }
	        System.out.println(sc.next());
		}
		sc.close();*/
	}

	/*function read
	 * determines given sentence as booleans and words
	 * input parameters: a word of the sentence
	 * output parameters: a word which is not an integer or boolean */
	private String read(String word) {
		if (isNumber(word)) {
			return "Such integer: " + word;
		} else if (word.equals("true") || word.equals("false")) {
			return "Such boolean: " + word;
		} else {
			return word;
		}
	}
/*
		if (!(sentence.trim()).isEmpty()) { //checks if input for sentence was given
			line = sentence;
			System.out.println(name + ": " + line);
			return true;
		} else {
			System.out.println(name + ": " + line);
			return false;
		}*/

	/*function isNumber
	 * determines if given word is an integer
	 * input parameters: a word
	 * output parameters: an int */
	private boolean isNumber(String aWord) {
		return aWord.matches("-?\\d+(\\.\\d+)?");
	}
}