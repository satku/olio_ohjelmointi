/**
 *
 */
package mainclass;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author elu
 *
 */
public class Mainclass {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String name;
		String dogSays;

        Scanner scan = new Scanner(System.in);

		System.out.print("Anna koiralle nimi: ");
		name = scan.nextLine();

		Dog dog1 = new Dog(name);
		
		System.out.print("Mitä koira sanoo: ");
		dogSays = scan.nextLine();
		//dogSays = br.readLine();
		
		dog1.speak(dogSays);
		scan.close();

/*
		while (true) {
			if (!dog1.speak(dogSays)) {
				System.out.print("Mitä koira sanoo: ");
				dogSays = scan.next();
			} else {
				break;
			}
		}*/

	}

}