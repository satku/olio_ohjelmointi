/**
 * @author elu - Esko Heino
 * Mainclass.java
 * Date: 5 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package tiedostonLuku;

import java.io.IOException;

/**
 *
 */
public class Mainclass {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		ReadAndWriteIO fileReading = new ReadAndWriteIO();
		fileReading.readAndWrite("zipinput.zip", "output.txt");

	}

}