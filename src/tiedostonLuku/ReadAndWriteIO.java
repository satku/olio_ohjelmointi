/**
 * @author elu - Esko Heino
 * ReadAndWriteIO.java
 * Date: 5 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package tiedostonLuku;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.zip.ZipInputStream;


/**
 *
 */
public class ReadAndWriteIO {

	public ReadAndWriteIO() {
	}

    public void readAndWrite(String loadFile, String saveFile) throws IOException {

        InputStream theFile = new FileInputStream(loadFile);
        ZipInputStream zipStream = new ZipInputStream(theFile);
        zipStream.getNextEntry();

        Scanner sc = new Scanner(zipStream);
        while (sc.hasNextLine()) {
            System.out.println(sc.nextLine());
        }
        sc.close();

//        BufferedReader in = new BufferedReader(new FileReader(loadFile));
//        BufferedWriter out = new BufferedWriter(new FileWriter(saveFile));
//        for(String line; (line = in.readLine()) != null; ) {
//        		System.out.println(line);
//        		if (line.trim().length() != 0 && line.length() < 30 && line.indexOf("v") != -1) {
//        			out.write(line + "\n");
//        		}
//        }
//        in.close();
//        out.close();
    }

}